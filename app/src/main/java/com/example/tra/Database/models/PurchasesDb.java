package com.example.tra.Database.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.tra.Database.core.DatabaseField;
import com.example.tra.Database.core.DatabaseHelper;


import java.util.ArrayList;

/**
 * Created by ua on 8/31/2018.
 */

public class PurchasesDb extends DatabaseHelper {

    public static final String TABLE_NAME = "artists";
    public static final String PRIMARY_KEY = "id";

    public static DatabaseField id           =  new DatabaseField("id",DatabaseField.INTEGER,11,false,true);
    public static DatabaseField name         =  new DatabaseField("name",DatabaseField.VARCHAR,30);
    public static DatabaseField joined       =  new DatabaseField("joined",DatabaseField.DATETIME,30,true);
    public static DatabaseField image        =  new DatabaseField("image",DatabaseField.BLOB,30,true);
    public static DatabaseField online_id    =  new DatabaseField("online_id",DatabaseField.INTEGER,11,true);

    Context context;

    public PurchasesDb(Context context) {
        super(context);
        this.context = context;
    }

    public PurchasesDb(Context context, SQLiteDatabase db) {
        super(context,db);
        this.context = context;
    }





}

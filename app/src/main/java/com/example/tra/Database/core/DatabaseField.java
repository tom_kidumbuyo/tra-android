package com.example.tra.Database.core;

import android.support.annotation.StringDef;
import android.util.Log;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.sql.Time;
import java.util.Date;

/**
 * Created by ua on 8/31/2018.
 */

public class DatabaseField  implements Cloneable {

    private static String TAG = "DatabaseField";

    public String Name = "Datafield";
    public String Type = INTEGER;
    public Integer Length = null;
    public Boolean Null = false;
    public Boolean PrimaryKey = false;
    public Boolean AutoIncrement = false;

    public Boolean isSet = false;

    public Integer intValue = null;
    public String stringValue = null;
    public Date dateValue = null;
    public Time timeValue = null;

    // Constants
    // Boolean
    public static final String TINYINT = "TINYINT"; // default length (1)

    // Intergers
    public static final String INTEGER = "INTEGER"; // default length (11)
    public static final String SMALLINT = "SMALLINT"; // default length (5)
    public static final String BIGINT = "BIGINT"; // default length (20)
    public static final String FLOAT = "FLOAT"; // default length (11)
    public static final String DOUBLE = "DOUBLE"; // default length (11)
    public static final String DECIMAL = "DECIMAL"; // default length (11)

    // Date and Time Types
    public static final String DATE = "DATE"; // default length (10)
    public static final String DATETIME = "DATETIME"; // default length (22)
    public static final String TIMESTAMP = "TIMESTAMP"; // default length (10)
    public static final String TIME = "TIME"; // default length (12)
    public static final String YEAR = "YEAR"; // default length (4)

    // text
    public static final String CHAR = "CHAR"; // default length ()
    public static final String VARCHAR = "VARCHAR"; // default length ()
    public static final String BLOB = "BLOB"; // default length ()
    public static final String TEXT = "TEXT"; // default length ()

    public static final String TINYBLOB = "TINYBLOB"; // default length ()
    public static final String TINYTEXT = "TINYTEXT"; // default length ()
    public static final String MEDIUMBLOB = "MEDIUMBLOB"; // default length ()
    public static final String MEDIUMTEXT = "MEDIUMTEXT"; // default length ()
    public static final String LONGBLOB = "LONGBLOB"; // default length ()
    public static final String LONGTEXT = "LONGTEXT"; // default length ()
    public static final String ENUM = "ENUM"; // default length ()


    // Declare the @ StringDef for these constants:
    @StringDef({INTEGER, TINYINT, SMALLINT, BIGINT, FLOAT, DOUBLE, DECIMAL, DATE, DATETIME, TIMESTAMP, TIME, YEAR, CHAR, VARCHAR, BLOB, TEXT, TINYBLOB, TINYTEXT ,MEDIUMBLOB, MEDIUMTEXT, LONGBLOB, LONGTEXT, ENUM })
    @Retention(RetentionPolicy.SOURCE)
    public @interface DbDataTypes {}

    public DatabaseField(String name, @DbDataTypes String type, Integer length, Boolean is_null, Boolean autoIncrement){
        Name = name;
        Type = type;
        Length = length;
        Null = is_null;
        AutoIncrement = autoIncrement;
    }
    public DatabaseField(String name, @DbDataTypes String type, Integer length, Boolean is_null){
        Name = name;
        Type = type;
        Length = length;
        Null = is_null;
        PrimaryKey = false;
        AutoIncrement = false;
    }
    public DatabaseField(String name, @DbDataTypes String type, Integer length){
        Name = name;
        Type = type;
        Length = length;
        Null = false;
        PrimaryKey = false;
        AutoIncrement = false;
    }
    public DatabaseField(String name, @DbDataTypes String type){
        Name = name;
        Type = type;
        Length = null;
        Null = false;
        PrimaryKey = false;
        AutoIncrement = false;
    }

    public DatabaseField(DatabaseField that) {

        this(that.Name, that.Type, that.Length, that.Null, that.AutoIncrement);

        isSet = false;
        intValue = null;
        stringValue = null;
        dateValue = null;
        timeValue = null;

    }

    public DatabaseField clone(){
        try {
            return (DatabaseField) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return new DatabaseField(this);
        }
    }

    public void set(DatabaseHelper dbObject, String value){
        if( Type ==  CHAR || Type ==  VARCHAR  || Type ==  TEXT || Type ==  TINYTEXT || Type ==  MEDIUMTEXT    || Type ==  LONGTEXT  || Type ==  ENUM ) {
            dbObject.stringValues.put(Name,value);
        }else {
            setDatatypeError();
        }
    }

    public void set(DatabaseHelper dbObject, Integer value){
        if(Type == INTEGER || Type == TINYINT  || Type == SMALLINT || Type ==  BIGINT || Type ==  FLOAT || Type ==  DOUBLE || Type ==  DECIMAL || Type == TIMESTAMP){
            dbObject.intValues.put(Name,value);
        }else if(Type ==  YEAR ){
            if(value < 9999){
                dbObject.intValues.put(Name,value);
            }else {
                // Log.e(TAG,"Database Feld year exceeded the maximum value 0f 9999. Please correct the year value");
            }
        }else {
            setDatatypeError();
        }
    }

    public void set(DatabaseHelper dbObject, Boolean value){
        if(Type == TINYINT){
            if(value == true){
                dbObject.intValues.put(Name,1);
                //dbObject.booleanValues.put(Name,value);
            }else {
                dbObject.intValues.put(Name,0);
                //dbObject.booleanValues.put(Name,value);
            }

        }
    }

    public void set(DatabaseHelper dbObject, Date value){
        // String StringDate = new SimpleDateFormat("yyyy-MM-dd").format(value);
        if(Type ==  DATE || Type ==  DATETIME ){
            dbObject.dateValues.put(Name,value);
        }else {
            setDatatypeError();
        }
    }

    public void set(DatabaseHelper dbObject, Time value){
        // String StringDate = new SimpleDateFormat("yyyy-MM-dd").format(value);
        if( Type ==  TIME ){
            dbObject.timeValues.put(Name,value);
        }else {
            setDatatypeError();
        }
    }

    public void set(DatabaseHelper dbObject, byte[] value) {
        // String StringDate = new SimpleDateFormat("yyyy-MM-dd").format(value);
        if( Type ==  BLOB || Type ==  LONGBLOB  || Type ==  TINYBLOB  || Type == MEDIUMBLOB ){
            dbObject.blobValues.put(Name, value);
        } else {
            setDatatypeError();
        }
    }


    public String getDataType(){
        if( Type ==  CHAR || Type ==  VARCHAR  || Type ==  TEXT || Type ==  TINYTEXT || Type ==  MEDIUMTEXT    || Type ==  LONGTEXT  || Type ==  ENUM ) {
            return "String";
        }else if(Type == INTEGER  || Type == SMALLINT || Type ==  BIGINT || Type ==  FLOAT || Type ==  DOUBLE || Type ==  DECIMAL || Type == TIMESTAMP || Type ==  YEAR ){
            return "Interger";
        }else if( Type == TINYINT ){
            return "Boolean";
        }else if(Type ==  DATE || Type ==  DATETIME ){
            return "Date";
        }else if( Type ==  TIME ){
            return "Time";
        }else if( Type ==  BLOB || Type ==  LONGBLOB  || Type ==  TINYBLOB  || Type == MEDIUMBLOB ){
            return "Blob";
        }else {
            return null;
        }
    }

    public void setDatatypeError(){

        if( Type ==  CHAR || Type ==  VARCHAR  || Type ==  TEXT || Type ==  TINYTEXT || Type ==  MEDIUMTEXT    || Type ==  LONGTEXT  || Type ==  ENUM ) {
            Log.e(TAG,Type + " type of this Database Field Requires a String as input. Use 'set(String value)'");
        }else if(Type == INTEGER  || Type == SMALLINT || Type ==  BIGINT || Type ==  FLOAT || Type ==  DOUBLE || Type ==  DECIMAL || Type == TIMESTAMP || Type ==  YEAR ){
            Log.e(TAG,Type + " type of this Database Field Requires a Interger as input. Use 'set(String value)'");
        }else if(Type == TINYINT ){
            Log.e(TAG,Type + " type of this Database Field Requires a Boolean as input. Use 'set(String value)'");
        }else if(Type ==  DATE  ){
            Log.e(TAG,Type + " type of this Database Field Requires a Date as input. Use 'set(Date value)'");
        }else if( Type ==  DATETIME  ){
            Log.e(TAG,Type + " type of this Database Field Requires a LocalDateTime as input. Use 'set(LocalDateTime value)'");
        }else if( Type ==  TIME ){
            Log.e(TAG,Type + " type of this Database Field Requires a Time as input. Use 'set(Time value)'");
        }else if( Type ==  BLOB || Type ==  LONGBLOB  || Type ==  TINYBLOB  || Type == MEDIUMBLOB ){
            Log.e(TAG,Type + " type of this Database Field Requires a Time as input. Use 'set(Time value)'");
        }else {

        }
    }



    public Object get(DatabaseHelper dbObject){

        if( Type ==  CHAR || Type ==  VARCHAR  || Type ==  TEXT || Type ==  TINYTEXT || Type ==  MEDIUMTEXT    || Type ==  LONGTEXT  || Type ==  ENUM ) {
            return dbObject.stringValues.get(Name);
        }else if(Type == INTEGER || Type == SMALLINT || Type ==  BIGINT || Type ==  FLOAT || Type ==  DOUBLE || Type ==  DECIMAL || Type == TIMESTAMP || Type ==  YEAR ){
            return dbObject.intValues.get(Name);
        }else if(Type == TINYINT ){
            if(dbObject.booleanValues.get(Name) == null){
                return false;
            }else {
                return dbObject.booleanValues.get(Name);
            }
        }else if(Type ==  DATE  || Type ==  DATETIME ){
            return dbObject.dateValues.get(Name);
        }else if( Type ==  TIME ){
            return dbObject.timeValues.get(Name);
        }else if( Type ==  BLOB || Type ==  LONGBLOB  || Type ==  TINYBLOB  || Type == MEDIUMBLOB ){
            return dbObject.blobValues.get(Name);
        }else {
            return false;
        }
    }








}

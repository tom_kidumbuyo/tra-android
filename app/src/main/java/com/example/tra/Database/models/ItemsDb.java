package com.example.tra.Database.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;


import com.example.tra.Database.core.DatabaseField;
import com.example.tra.Database.core.DatabaseHelper;

import java.util.ArrayList;

/**
 * Created by ua on 8/31/2018.
 */

public class ItemsDb extends DatabaseHelper {

    public static final String TABLE_NAME = "playlists";
    public static final String PRIMARY_KEY = "id";

    public static DatabaseField id          = new DatabaseField("id",DatabaseField.INTEGER,11,false,true);
    public static DatabaseField name        = new DatabaseField("song",DatabaseField.CHAR,30,false);
    public static DatabaseField time        = new DatabaseField("time",DatabaseField.TIME,30,true);

    Context context;

    public ItemsDb(Context context) {

        super(context);
        this.context = context;
    }

    public ItemsDb(Context context, SQLiteDatabase db) {
        super(context,db);
        this.context = context;
    }





}

package com.example.tra.Database.core;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.example.tra.MyApplication;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ua on 8/31/2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper {


    public static final String DATABASE_NAME = "wasafi.db";
    public String TABLE_NAME;
    public String PRIMARY_KEY;

    private SQLiteDatabase db;
    private Context main_context;
    private Cursor res;

    public Map<String, Integer> intValues = new HashMap<String, Integer>();
    public Map<String, String> stringValues = new HashMap<String, String>();
    public Map<String, Date> dateValues = new HashMap<String, Date>();
    public Map<String, Time> timeValues = new HashMap<String, Time>();
    public Map<String, byte[]> blobValues = new HashMap<String, byte[]>();
    public Map<String, Boolean> booleanValues = new HashMap<String, Boolean>();



    public DatabaseHelper(Context context) {

        super(context, DATABASE_NAME, null, 1);
        if(db == null) {
            try {
                db = ((MyApplication) ((Activity) context).getApplication()).getDb();
                if (db == null) {
                    db = this.getWritableDatabase();
                    ((MyApplication) ((Activity) context).getApplication()).setDb(db);
                }
            } catch (Exception e) {
                db = this.getWritableDatabase();
            }
        }

        main_context = context;
        setPrimaryKey();
        if(!isTableExists()){
            createTable(db);
        }
    }

    public DatabaseHelper(Context context, SQLiteDatabase thisDb) {
        super(context, DATABASE_NAME, null, 1);
        db = thisDb;
        main_context = context;
        setPrimaryKey();
        if(!isTableExists()){
            createTable(db);
        }
    }

    private void setPrimaryKey(){
        for (Field field: getClass().getDeclaredFields()) {
            if(field.getName() == "PRIMARY_KEY"){
                try {
                    PRIMARY_KEY = (String) field.get(this);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            if(field.getName() == "TABLE_NAME"){
                try {
                    TABLE_NAME = (String) field.get(this);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        if(!isTableExists()){
            createTable(db);
        }
    }

    public boolean isTableExists() {
        if(TABLE_NAME != null) {
            Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = \"" + TABLE_NAME + "\"", null);
            if (cursor != null) {
                try{
                    if (cursor.getCount() > 0) {
                        cursor.close();
                        return true;
                    }
                    cursor.close();
                }catch (SQLException e){
                    db.close();
                    db = this.getWritableDatabase();
                    Log.e("HomeActivity",e.getMessage());
                    return isTableExists();
                }

            }
            return false;
        }
        return false;
    }

    public void createTable(SQLiteDatabase db) {
        String fields_string = ""; int  i = 0;
        for (Field field: getClass().getDeclaredFields()) {
            if(field.getName() == "PRIMARY_KEY"){
                try {
                    PRIMARY_KEY = (String) field.get(this);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        for (Field field: getClass().getDeclaredFields()) {
            if (field.getType().toString().contains("DatabaseField")) {

                DatabaseField field_object = null;
                try {

                    Field ffield = this.getClass().getDeclaredField(field.getName());
                    ffield.setAccessible(true);
                    field_object = (DatabaseField) ffield.get(this);

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    Log.e("Homeactivity", "error1: "+ e.getMessage());
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                    Log.e("Homeactivity", "error2: "+ e.getMessage());
                }


                if (i != 0) {
                    fields_string += " , ";
                }

                fields_string += " " + field.getName() + " " + field_object.Type;

                if (field_object.Type == "VARCHAR" || field_object.Type == "CHAR") {
                        if (field_object.Length != null) {
                            fields_string += "(" + field_object.Length.toString() + ")";
                        } else {
                            fields_string += "(225)";
                        }
                } else if (field_object.Type == "INTEGER") { // default length (11)
//                    if (field_object.Length != null) {
//                        fields_string += "(" + field_object.Length.toString() + ")";
//                    } else {
//                        fields_string += "(11)";
//                    }
                } else if (field_object.Type == "DATE") { // default length (10)
                        fields_string += "(10)";
                } else if (field_object.Type == "DATETIME") { // default length (22)
                        fields_string += "(22)";
                } else if (field_object.Type == "TIMESTAMP") { // default length (10)
                        fields_string += "(10)";
                } else if (field_object.Type == "TIME") { // default length (12)
                        fields_string += "(12)";
                } else if (field_object.Type == "YEAR") { // default length (4)
                        fields_string += "(4)";
                }


                if (!field_object.Null)
                    fields_string += " NOT NULL";

                if (field.getName() == PRIMARY_KEY || field_object.AutoIncrement)
                    fields_string += " PRIMARY KEY";

                if (field_object.AutoIncrement)
                    fields_string += " AUTOINCREMENT";
                i++;

            }else if(field.getName() == "TABLE_NAME"){

                try {
                    TABLE_NAME = (String) field.get(this);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        String sql = "CREATE TABLE IF NOT EXISTS "+TABLE_NAME+" ("+fields_string+")";

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }

    public DatabaseField getPrimaryKey(){
        DatabaseField value = null;
        for (Field field: getClass().getDeclaredFields()) {
            if (field.getType().toString().contains("DatabaseField")){
                if (field.getName() == PRIMARY_KEY) {
                    try {
                        value = (DatabaseField) field.get(this);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return value;
    }

    public boolean isNew(){
        DatabaseField field = getPrimaryKey();
        if(field.get(this) == null){
            return true;
        }else{
            return false;
        }
    }

    public boolean save()  {
        if(isNew()){
            if(insert() != null){
                return true;
            }else {
                return false;
            }
        }else{
            if(update() != null){
                return true;
            }else {
                return false;
            }
        }
    }

    public DatabaseHelper update(){
        ContentValues contentValues = new ContentValues();

        for (Field field: getClass().getDeclaredFields()) {
            if (field.getType().toString().contains("DatabaseField")){
                DatabaseField value = null;
                try {
                    value = (DatabaseField) field.get(this);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                if(value.get(this) != null) {
                    if (value.getDataType() == "String") {
                        contentValues.put(field.getName(), (String) value.get(this));
                    } else if (value.getDataType() == "Interger") {
                        contentValues.put(field.getName(), (Integer) value.get(this));
                    } else if (value.getDataType() == "Date") {
                        String StringDate = new SimpleDateFormat("yyyy-MM-dd").format(value.get(this));
                        contentValues.put(field.getName(), (String) StringDate);
                    } else if (value.getDataType() == "Date") {
                        String StringDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(value.get(this));
                        contentValues.put(field.getName(), (String) StringDate);
                    } else if (value.getDataType() == "Time") {
                        String StringTime = new SimpleDateFormat("HH:mm:ss").format(value.get(this));
                        contentValues.put(field.getName(), (String) StringTime);
                    }
                }
            }
        }
        int result_id = db.update(TABLE_NAME,contentValues,PRIMARY_KEY +" = ?",new String[] {(String) this.getPrimaryKey().get(this).toString()});
        this.load((int) result_id);
        return this;
    }

    public DatabaseHelper insert() {
        ContentValues contentValues = new ContentValues();
        for (Field field : getClass().getDeclaredFields()) {
            if (field.getType().toString().contains("DatabaseField") && field.getName() != PRIMARY_KEY) {
                DatabaseField value = null;
                try {
                    value = (DatabaseField) field.get(this);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                if (value.get(this) != null) {
                    if (value.getDataType() == "String") {
                        contentValues.put(field.getName(), (String) value.get(this));
                    } else if (value.getDataType() == "Interger") {
                        contentValues.put(field.getName(), (Integer) value.get(this));
                    } else if (value.getDataType() == "Date") {
                        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                        Date date = (Date) value.get(this);
                        String outputText = outputFormat.format(date);
                        contentValues.put(field.getName(), (String) outputText);
                    } else if (value.getDataType() == "Time") {
                        String StringTime = new SimpleDateFormat("HH:mm:ss").format(value.get(this));
                        contentValues.put(field.getName(), (String) StringTime);
                    }
                }else{
                    Log.e("Database Helper", "this value is null");
                }
            }
        }

        try {
            long result_id = db.insertOrThrow(TABLE_NAME, null, contentValues);
            this.load((int) result_id);
            return this;
        }catch (SQLException e){
            Log.e("Homeactivity", "Database error: " + e.getMessage());
            return null;
        }

    }

    public boolean delete(){
        db.delete(TABLE_NAME,PRIMARY_KEY+" = ?",new String[] {(String) this.getPrimaryKey().get(this).toString()});
        return true;
    }

    private DatabaseHelper populate(DatabaseHelper DbObject){

            for (Field field : DbObject.getClass().getDeclaredFields()) {

                    if (field.getType().toString().contains("DatabaseField")) {
                        DatabaseField value = null;
                        try {
                            value = new DatabaseField((DatabaseField) field.get(DbObject));
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                        int i = res.getColumnIndex(field.getName());

                        if(i < res.getColumnCount() && i != -1) {

                            if (value.getDataType() == "String" ) {
                                value.set(DbObject,res.getString(i));
                            } else if (value.getDataType() == "Interger" ) {
                                value.set(DbObject,res.getInt(i));
                            } else if (value.getDataType() == "Date") {
                                Date date = new Date(res.getLong(i));
                                value.set(DbObject,date);
                            } else if (value.getDataType() == "Date") {
                                Date date = new Date(res.getLong(i));
                                value.set(DbObject,date);
                            } else if (value.getDataType() == "Time") {
                                Time time = new Time(res.getLong(i));
                                value.set(DbObject,time);
                            }else if (value.getDataType() == "Blob") {
                                byte[] blob = res.getBlob(i);
                                value.set(DbObject,blob);
                            }

                            try {
                                field.set(DbObject,value);
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }

                        }else {


                                DatabaseField field_object = null;
                                try {

                                    Field ffield = this.getClass().getDeclaredField(field.getName());
                                    ffield.setAccessible(true);
                                    field_object = (DatabaseField) ffield.get(this);

                                } catch (IllegalAccessException e) {
                                    e.printStackTrace();
                                    Log.e("Homeactivity", "error1: "+ e.getMessage());
                                } catch (NoSuchFieldException e) {
                                    e.printStackTrace();
                                    Log.e("Homeactivity", "error2: "+ e.getMessage());
                                }

                                String fields_string = " " + field.getName() + " " + field_object.Type;

                                if (field_object.Type == "VARCHAR" || field_object.Type == "CHAR") {
                                    if (field_object.Length != null) {
                                        fields_string += "(" + field_object.Length.toString() + ")";
                                    } else {
                                        fields_string += "(225)";
                                    }
                                } else if (field_object.Type == "INTEGER") { // default length (11)
//                    if (field_object.Length != null) {
//                        fields_string += "(" + field_object.Length.toString() + ")";
//                    } else {
//                        fields_string += "(11)";
//                    }
                                } else if (field_object.Type == "DATE") { // default length (10)
                                    fields_string += "(10)";
                                } else if (field_object.Type == "DATETIME") { // default length (22)
                                    fields_string += "(22)";
                                } else if (field_object.Type == "TIMESTAMP") { // default length (10)
                                    fields_string += "(10)";
                                } else if (field_object.Type == "TIME") { // default length (12)
                                    fields_string += "(12)";
                                } else if (field_object.Type == "YEAR") { // default length (4)
                                    fields_string += "(4)";
                                }

                                if (!field_object.Null)
                                    fields_string += " NOT NULL";

                                if (field.getName() == PRIMARY_KEY || field_object.AutoIncrement)
                                    fields_string += " PRIMARY KEY";

                                if (field_object.AutoIncrement)
                                    fields_string += " AUTOINCREMENT";

                            String query = "ALTER TABLE " + TABLE_NAME + " ADD "+fields_string;
                            Log.e("Homeactivity","Query : "+query);

                            db.execSQL(query);

                            Log.e("Homeactivity","Name :"+value.Name);
                            Log.e("Homeactivity","i :"+i);
                            Log.e("Homeactivity","Error : field initiation error: ");
                        }
                    }
                    // i++;
            }
            return DbObject;
    }

    private ArrayList<?> populateList(){

        if(res.moveToFirst()) {

            List<DatabaseHelper> resultList = new ArrayList<>();

            do{


                DatabaseHelper populated_object = null;
                try {
                    populated_object = populate(getClass().getDeclaredConstructor(Context.class, SQLiteDatabase.class).newInstance(main_context,db));
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
                if(populated_object != null ) {
                    resultList.add(populated_object);
                }
            }while (res.moveToNext());

            res.close();
            return (ArrayList<DatabaseHelper>) resultList;

        }else {
            return new ArrayList<>();
        }

    }

    public ArrayList<?> query(String sql) {
        SQLiteDatabase db = this.getWritableDatabase();
        res = db.rawQuery(sql,null);
        return  populateList();
    }

    public ArrayList<?> where(String sql){
        res = db.rawQuery("SELECT * FROM `"+TABLE_NAME+"` WHERE "+sql,null);
        return  populateList();
    }

    public ArrayList<?> getAll() {
        res = db.rawQuery("SELECT * FROM `"+TABLE_NAME+"`",null);
        if(res.getCount() > 0) {
            return populateList();
        }else{
            return null;
        }
    }

    public ArrayList<?> getAllSort(String string) {
        res = db.rawQuery("SELECT * FROM `"+TABLE_NAME+"` ORDER BY `"+string+"` ASC",null);
        if(res.getCount() > 0) {
            return populateList();
        }else{
            return null;
        }
    }

    public ArrayList<?> getAllSort(String string, Boolean order) {
        String orderBy = "ASC";
        if(!order)
            orderBy = "DESC";

        res = db.rawQuery("SELECT * FROM `"+TABLE_NAME+"` ORDER BY `"+string+"` "+orderBy,null);
        if(res.getCount() > 0) {
            return populateList();
        }else{
            return null;
        }
    }

    public Object load(Integer id) {
        res = db.rawQuery("select * from `"+TABLE_NAME+"` WHERE "+PRIMARY_KEY+" = "+id+"",null);
        if(res.moveToFirst()) {
            populate(this);
            res.close();
            return this;
        }else {
            return null;
        }
    }


}

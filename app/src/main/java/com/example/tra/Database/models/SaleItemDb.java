package com.example.tra.Database.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.tra.Database.core.DatabaseField;
import com.example.tra.Database.core.DatabaseHelper;


import java.util.ArrayList;

/**
 * Created by ua on 8/31/2018.
 */

public class SaleItemDb extends DatabaseHelper {

    public static final String TABLE_NAME = "playlistsongs";
    public static final String PRIMARY_KEY = "id";

    public static DatabaseField id          =  new DatabaseField("id",DatabaseField.INTEGER,11,false,true);
    public static DatabaseField song        = new DatabaseField("song",DatabaseField.INTEGER,11,false);
    public static DatabaseField playlist    = new DatabaseField("playlist",DatabaseField.INTEGER,11,false);
    public static DatabaseField time        = new DatabaseField("time",DatabaseField.TIME,11,true);


    public SaleItemDb(Context context) {
        super(context);
    }

    public SaleItemDb(Context context, SQLiteDatabase db) {
        super(context,db);
    }





}
